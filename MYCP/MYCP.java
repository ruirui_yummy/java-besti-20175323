import java.io.*;
public class MYCP {
    public static void dumpToTwo(InputStream src, OutputStream dest)
            throws IOException {
        try (InputStream input = src; OutputStream output = dest) {
            byte[] data = new byte[1];
            int length;
            while ((length = input.read(data)) != -1) {
                String str = Integer.toBinaryString((data[0]&0xFF)+0x100).substring(1);
                data[0] = Byte.parseByte(str);
                output.write(data, 0, length);
            }
        }
    }
    public static void dumpToTen(InputStream src, OutputStream dest)
            throws IOException {
        try (InputStream input = src; OutputStream output = dest) {
            byte[] data = new byte[1];
            int length;
            while ((length = input.read(data)) != -1) {
                data[0] = Byte.parseByte(String.valueOf(data[0]),10);
                output.write(data, 0, length);
            }
        }
    }
    public static void main(String[] args) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
           fis = new FileInputStream("C:/Java/from.txt");
            fos = new FileOutputStream("C:/Java/to.txt");
           dumpToTen(fis, fos);
        }catch(Exception e) {
            System.out.println(e);
        }
    }
}
