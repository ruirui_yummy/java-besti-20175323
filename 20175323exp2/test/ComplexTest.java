import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex a = new Complex(2,3);Complex b = new Complex(1,4);Complex c = new Complex(3,4);
    @Test
    public void testAdd(){       //测试加法
        assertEquals(3.0,a.ComplexAdd(b).RealPart);
        assertEquals(4.0,c.ComplexAdd(b).RealPart);
    }
    public void testSub(){       //测试减法
        assertEquals(-2.0,b.ComplexSub(c).RealPart);
        assertEquals(0.0,c.ComplexSub(b).ImagePart);
    }
    public void testMulti(){     //测试乘法
        assertEquals(-10.0,a.ComplexMulti(b).RealPart);
        assertEquals(11.0,a.ComplexMulti(b).ImagePart);
    }
    public void testDiv(){      //测试除法
        assertEquals(19.0/25.0,b.ComplexDiv(c).RealPart);
        assertEquals(8.0/25.0,b.ComplexDiv(c).ImagePart);
    }
    public void testtoString(){
        assertEquals("1.0+4.0i",b.toString());
    }
    public void testequals(){
        assertEquals(false,a.equals(b));
    }
}
