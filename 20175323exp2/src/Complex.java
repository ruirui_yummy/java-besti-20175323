public class Complex {
    // 定义属性并生成getter,setter
    double RealPart;
    double ImagePart;
    public void setRealPart(double m){
        RealPart = m;
    }
    public void setImagePart(double n){
        ImagePart = n;
    }
    public double getRealPart(){
        return RealPart;
    }
    public double getImagePart(){
        return ImagePart;
    }
    // 定义构造函数
    public Complex(){}
    public Complex(double R,double I){
        RealPart=R;
        ImagePart=I;
    }
    //Override Object
    public boolean equals(Object obj){
        if(this==obj) return true;
        else if(obj==null) return false;
        else{
            obj=(Complex)obj;
            double r = ((Complex)obj).getRealPart();
            double i = ((Complex)obj).getImagePart();
            if((r==RealPart)&&(i==ImagePart)){
                return true;
            }
            else return false;
        }
    };
    public String toString(){
        return RealPart+"+"+ImagePart+"i";
    };

    // 定义公有方法:加减乘除
    public Complex ComplexAdd(Complex a){

        return new Complex(RealPart+a.RealPart,ImagePart+a.ImagePart);
    };
    public Complex ComplexSub(Complex a){

        return new Complex(RealPart-a.RealPart,ImagePart-a.ImagePart);
    };
    public Complex ComplexMulti(Complex a){
        return new Complex(RealPart*a.RealPart-ImagePart*a.ImagePart,
                RealPart*a.ImagePart+ImagePart*a.RealPart);
    };
    public Complex ComplexDiv(Complex a){
        return new Complex((RealPart*a.RealPart+ImagePart*a.ImagePart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart),
                (ImagePart*a.RealPart-RealPart*a.ImagePart)/(a.RealPart*a.RealPart+a.ImagePart*a.ImagePart));
    };
}

