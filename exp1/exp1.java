import java.util.Scanner;
public class exp1 {
    public static void main(String[] args) {
        System.out.println("请输入排列数中A(n,m)中的n和m，输入一个数后用回车确定");
        Scanner reader = new Scanner(System.in);
        int n = reader.nextInt();
        int m = reader.nextInt();
        if(m==n){
            System.out.println("排列数的结果是：" + 1);
        }
        else if(m>0&&n>0){
        System.out.println("排列数的结果是：" + arrangement(n, m));
        }
        else
            System.out.println("输入错误");
    }
    private static long factorial(int n) {
            return (n > 1) ? n * factorial(n - 1) : 1;
        }/*计算一个数的阶乘*/
    public static long arrangement(int n, int m) {
            return (n >= m) ? factorial(n) / factorial(n - m) : 0;
        }/*计算排列数*/
}
